<?php

use Solvam\SmsTools\GsmSmsEncoding;

class GsmSmsEncodingTest extends PHPUnit_Framework_TestCase
{
	/** @test */
	public function calculates_correct_length_for_gsm_basic_chars()
	{
		$data = [
			'a' => 1,
			'abcdef' => 6,
			'AÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ' => 18,
		];

		foreach($data as $str => $len) {
			$this->assertEquals($len, GsmSmsEncoding::gsm7bitLength($str));
		}
	}

	/** @test */
	public function calculates_correct_length_for_gsm_mixed()
	{
		$data = [
			'a^' => 3,
			'abcdef{}' => 10,
			'AÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ@@|€' => 24,
		];

		foreach($data as $str => $len) {
			$this->assertEquals($len, GsmSmsEncoding::gsm7bitLength($str));
		}
	}

	/** @test */
	public function calculates_correct_length_for_gsm_extended_chars()
	{
		$data = [
			'^' => 2,
			'^{}\\[~]|€' => 18
		];

		foreach($data as $str => $len) {
			$this->assertEquals($len, GsmSmsEncoding::gsm7bitLength($str));
		}
	}

	/** @test */
	public function calculates_correct_length_for_really_long_mixed_strings()
	{
		$str = $this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 1000);
		$str .= $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 500);
		$this->assertEquals(2000, GsmSmsEncoding::gsm7bitLength($str));
	}

	/** @test */
	public function returns_error_if_non_gsm_characters_given()
	{
		// this could use a better fuzzer...
		$this->assertEquals(-1, GsmSmsEncoding::gsm7bitLength('á'));
		$this->assertEquals(-1, GsmSmsEncoding::gsm7bitLength('😀'));
	}

	/** @test */
	public function calculates_correct_length_for_ucs2_with_gsmonly_chars()
	{
		$data = [
			'a^' => 2,
			'abcdef{}' => 8,
			'AÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ@@|€' => 22,
		];

		foreach($data as $str => $len) {
			$this->assertEquals($len, GsmSmsEncoding::gsmUcs2Length($str));
		}
	}

	/** @test */
	public function calculates_correct_length_for_ucs2_with_ucs2_chars()
	{
		// this could use a better fuzzer as well
		$data = [
			'a^' => 2,
			'abcdef{}' => 8,
			'AÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ@@|€' => 22,
			'あいうえお' => 5
		];

		foreach($data as $str => $len) {
			$this->assertEquals($len, GsmSmsEncoding::gsmUcs2Length($str));
		}
	}

	/** @test */
	public function calculates_correct_length_for_ucs2_with_non_ucs2_chars()
	{
		// this could use a better fuzzer as well
		$data = [
			'😀' => 2,
			'😀😀😀' => 6
		];

		foreach($data as $str => $len) {
			$this->assertEquals($len, GsmSmsEncoding::gsmUcs2Length($str));
		}
	}


	/** @test */
	public function calculates_length_of_7bit_one_part_messages()
	{
		$strs = [];
		$strs[] = 'a';
		$strs[] = '^';
		$strs[] = $this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 160);
		$strs[] = $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 80);
		$strs[] = $this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 100)
			. $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 30);

		foreach($strs as $str) {
			$this->assertEquals(1, GsmSmsEncoding::multipartLength($str));
		}
	}

	/** @test */
	public function calculates_length_of_7bit_multi_part_messages()
	{
		$strs = [
			// 161-length
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 161) => 2,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 80).'a' => 2,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 101)
				. $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 30) => 2,
			
			// 306-length
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 306) => 2,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 153) => 2,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 106)
			 	. $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 100) => 2,

			// 307-length
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 307) => 3,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 153).'a' => 3,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 107)
				. $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 100) => 3,

			// 459-length
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 459) => 3,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 229).'a' => 3,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 256)
				. $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 100) => 3,

			// 460-length
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 460) => 4,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 230) => 4,
			$this->generateString(GsmSmsEncoding::GSM_7BIT_BASIC, 60)
				. $this->generateString(GsmSmsEncoding::GSM_7BIT_EXTENDED, 200) => 4
		];

		foreach($strs as $str => $parts) {
			$this->assertEquals($parts, GsmSmsEncoding::multipartLength($str), $str);
		}
	}

	/** @test */
	public function calculates_length_of_ucs2_one_part_messages()
	{
		// better charset would be nice
		$chars = 'あいうえおáü ';
		$strs = [
			'á',
			$this->generateString('a', 69).'á', // normal ascii but with ONE extra char
			$this->generateString($chars, 70)
		];

		foreach($strs as $str) {
			$this->assertEquals(1, GsmSmsEncoding::multipartLength($str));
		}
	}

	/** @test */
	public function calculates_length_of_ucs2_multi_part_messages()
	{
		$chars = 'あいうえおáü ';

		$strs = [
			// 71 length
			$this->generateString($chars, 71) => 2,
			$this->generateString('abc', 70).'á' => 2, // ascii but with ONE extra char
			
			// 134-length
			$this->generateString($chars, 134) => 2,
			$this->generateString('abc', 133).'á' => 2, // ascii but with ONE extra char

			// 135-length
			$this->generateString($chars, 135) => 3,
			$this->generateString('abc', 134).'á' => 3, // ascii but with ONE extra char

			// 201-length
			$this->generateString($chars, 201) => 3,
			$this->generateString('abc', 200).'á' => 3, // ascii but with ONE extra char

			// 202-length
			$this->generateString($chars, 202) => 4,
			$this->generateString('abc', 201).'á' => 4 // ascii but with ONE extra char
		];

		foreach($strs as $str => $parts) {
			$this->assertEquals($parts, GsmSmsEncoding::multipartLength($str));
		}
	}

	/** @test */
	public function can_determine_whether_message_is_gsm_7bit_encodable()
	{
		$message = 'utf=no';
		$this->assertTrue(GsmSmsEncoding::isGsm7bitEncodable($message));

		$message = "utf8=✓";
		$this->assertFalse(GsmSmsEncoding::isGsm7bitEncodable($message));
	}

	protected function generateString($charset, $length) {
		$charset_size = mb_strlen($charset, 'UTF-8');
		
		$chars = [];
		for($i = 0; $i < $length; $i++) {
			$index = rand(0, $charset_size-1);
			$char = mb_substr($charset, $index, 1);
			$chars[] = $char;
		}
		return implode('', $chars);
	}
}